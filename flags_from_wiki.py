#!/usr/bin/python
#                                                         -*- coding: utf8 -*-

from lxml import html
import requests


def getContent():
    try:
        print('Read cached data')
        f = open("ISO_3166-1.html", "rb")
        return f.read()

    except FileNotFoundError:
        print('Download data')
        f = open("ISO_3166-1.html", "wb")
        page = requests.get('https://ru.wikipedia.org/wiki/ISO_3166-1')
        content = page.content
        f.write(content)
        return content

tree = html.fromstring(getContent())
for tr in tree.xpath('//table[@class="wikitable sortable"]/tr'):
    country, A2, A3, num, geocode = tr.getchildren()
    if country.tag == 'th': continue  # варварство, но лень сочинять xpath
    name = country.xpath('.//a/@title')[0]
    thumb = 'http:' + country.xpath('.//img/@src')[0]
    image = thumb.replace('/thumb', '')
    image = image[:image.find('.svg')] + '.svg'
    open('%s.png' % A3.text, 'wb').write(requests.get(thumb).content)
    open('%s.svg' % A3.text, 'wb').write(requests.get(image).content)

